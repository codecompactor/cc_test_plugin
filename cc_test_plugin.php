<?php
/**
* Plugin Name: Codecompactor Test Plugin
* Plugin URI: http://codecompactor.com/
* Description: A plugin to demonstrate the capabilities of the CodeCompactor update service
* Author: Judah Wright
* Version: 0.2.1
* Author URI: http://judahwright.me
*/

require_once 'vendor/autoload.php';

\CodeCompactor\WordPressUpdateClient\UpdateClient::invokeUpdateChecker(__FILE__, '1\/ShksnTCFy3A5bo8x');
